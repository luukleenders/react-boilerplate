import styled from 'styled-components'

export const Title = styled.h1`
  font-size: 1.5rem;
`;

export const Text = styled.p`
  font-size: 1rem;
`;