import { createGlobalStyle } from 'styled-components'

declare module 'styled-components' {
  export interface DefaultTheme {
    typography: typeof typography
    body: string
  }
}

const typography = {
  fontSize: 18,
  fontFamily: 'sans-serif'
};

const colors = {
  primaryLight: '#f6f6f6'
}

export const lightTheme = {
  typography,
  body: colors.primaryLight
};

export const GlobalStyles = createGlobalStyle`
  html,
  body {
    height: 100%;
    padding: 0;
    margin: 0;
    background: ${({ theme }) => theme.body};
    font-family: ${({ theme }) => theme.typography.fontFamily};
    font-size: ${({ theme }) => theme.typography.fontSize}px;
  }

  #__next {
    height: 100%;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }
`
