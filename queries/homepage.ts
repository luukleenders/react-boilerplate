import { gql } from '@apollo/client'
import client from '../apollo-client'

interface HomepageProps {
  title: string;
  text: string;
}

interface HomepageQueryResult {
  homepage: {
    data: {
      attributes: HomepageProps;
    }
  }
}

const mapHomepageProps = ({ homepage }: HomepageQueryResult): HomepageProps => (
  {
    title: homepage.data.attributes.title,
    text: homepage.data.attributes.text
  }
);

// Example query
const HOMEPAGE = gql`
  query Homepage {
    homepage {
      title
      text
    }
  }
`;

export const homepageQuery = async () => {
  try {
    const { data } = await client.query({ query: HOMEPAGE });
    const props = mapHomepageProps(data);

    return props;
  } catch(error) {
    console.debug('Server error, returning mock data.')

    const props: HomepageProps = {
      title: 'Hello world!',
      text: 'React Boilerplate, using React, styled-components, NextJS, Apollo Client'
    }

    return props; 
  }
}