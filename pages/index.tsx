import React from 'react'
import Head from 'next/head'
import styled from 'styled-components'

import type { GetServerSideProps, InferGetServerSidePropsType } from 'next'

import { homepageQuery } from '@/queries'
import { Title, Text } from '@/components/typography'

export const getServerSideProps: GetServerSideProps = async (context) => {
  const props = await homepageQuery();
  return { props } 
}

export default (props: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { title, text } = props;
  
  return (
    <React.Fragment>
      <Head>
        <title>Homepage</title>
      </Head>

      <Main>
        <Title>{ title }</Title>
        <Text>{ text }</Text>
      </Main>
    </React.Fragment>
  );
}

const Main = styled.main`
  display: flex;
  height: 100%;
  flex-flow: column wrap;
  align-items: center;
  justify-content: center;
`;