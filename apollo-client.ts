import { ApolloClient, InMemoryCache } from '@apollo/client';

const protocol = process.env.REACT_APP_GRAPHQL_PROTOCOL;
const host = process.env.REACT_APP_GRAPHQL_HOST;
const port = process.env.REACT_APP_GRAPHQL_PORT;
const route = process.env.REACT_APP_GRAPHQL_ROUTE;
const token = process.env.REACT_APP_GRAPHQL_API_TOKEN;

const client = new ApolloClient({
  uri: `${protocol}://${host}:${port}/${route}`,
  cache: new InMemoryCache(),
  headers: {
    authorization: token ? `Bearer ${token}` : "",
  }
});

export default client;
